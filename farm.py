# coding: utf-8
import asyncio
import base64
import json
import random
import time
import datetime
import re
import rsa
import aiohttp
from Crypto.Cipher import AES
from rsa import core, PublicKey, transform

"""
@ 下载链接微信打开  http://w.jqsjgwb.cn/wx/s?_co=797679&_st=tideNx&_v=v6
@ 账号配置在 Farming_immortality.json    文件里面
@ 版本 1.3
@ cron: */5 * * * *
@ new Env('种田做饭')
"""

################################################配置区域#########################################################

################################################配置区域#########################################################
################################################下面的不要动#########################################################
vi = '106005'  # ua版本  默认201070
version = '1.6.5'  # 潮玩版本
host = 'https://android-api.lucklyworld.com'


async def __headers__(token):
        try:

            headers = {
                'User-Agent': f'com.ainimei.farmworld/{version} (Linux; U; Android 9; zh-cn) (official; {vi})',
                'Channel': 'official',
                'DEVICEID': token['DEVICEID'],
                'ANDROIDID': token['ANDROIDID'],
                'oaid': token['oaid'],
                'uid': token['uid'],
                'token': token['token'],
                'Host': 'android-api.lucklyworld.com',
            }
            return headers
        except Exception as e:
            print(e)


# 账号信息
async def user_info(token, len_new):
        try:
            resp = json.loads(await post_json(f'{host}/api/user/info?uid={token["uid"]}&version={version}',headers=await __headers__(token), data={}))
            # print(resp)
            r2 = json.loads(await post_json(f'{host}/v8/api/farm/finance?uid={token["uid"]}&version={version}',headers=await __headers__(token), data={}))
            # print(r2)
            if 'errorCode' in resp:
                return False
            if 'userId' in resp:
                userId = resp['userId']             # id
                di = str(userId)[0:2] + '**' + str(userId)[4:]
                nickname = resp['nickname'][:4]         # 名字
                copperNum = r2['copperNum']             # 铜钱
                print(f"[账号{len_new + 1}]信息->ID:{di}丨昵称:{nickname}丨铜钱:{copperNum}")
            return True
        except Exception as e:
            print(e)


# 收获农作物
async def farm_data(token, len_new):
    try:
        r1 = json.loads(await post_json(f'{host}/v8/api/farm/data?uid={token["uid"]}&version={version}',headers=await __headers__(token), data={}))
        # print(r1)
        for i in r1['farm']:
            material = i['material']
            id_new = i['id']
            if material:
                # print(id_new)
                if i['material']['seconds'] < 1:
                    await material_harvest(token, len_new, id_new)
    except Exception as e:
        print(e)


# 收获作物动物
async def material_harvest(token, len_new, id_new):
    try:
        b2 = {"estateId": str(id_new)}
        r2 = json.loads(await post_json(f'{host}/v8/api/farm/material/harvest?uid={token["uid"]}&version={version}', headers=await __headers__(token), data=b2))
        # print(r2)
        if 'name' in r2:
            print(f"[账号{len_new + 1}]收获->{r2['name']}")
            return True
        return False
    except Exception as e:
        print(e)


# 种植农作物
async def farm_material(token, len_new, id_new, order):
    try:
        b2 = {"estateId": str(id_new), "materialId": str(order)}
        r2 = json.loads(
            await post_json(f'{host}/v8/api/farm/material/sow?uid={token["uid"]}&version={version}',
                            headers=await __headers__(token), data=b2))
        # print(r2)
        if 'seconds' in r2:
            print(f"[账号{len_new + 1}]种植->{id_new}号土地种植成功丨ID:{order}丨{r2['seconds'] / 60}分钟成熟")
            return 1
        else:
            print(r2)
    except Exception as e:
        print(e)

# 田块状态
async def farm_kitchen(token, len_new):
    global order_all
    try:
        order_all = []
        b2 = {"next": "0"}
        r2 = json.loads(await post_json(f'{host}/v8/api/farm/product?uid={token["uid"]}&version={version}', headers=await __headers__(token), data=b2))
        # print(r2)
        for _ in list(reversed(r2['items']))[:4]:
            for p in _['compound']:
                holdNum = p['holdNum']
                if holdNum < 1:
                    for o in range(5):
                        if p['id'] < 1100:
                            order_all.append(p['id'])
                if p['id'] < 1100:
                    order_all.append(p['id'])
        # print(order_all)
        r1 = json.loads(await post_json(f'{host}/v8/api/farm/data?uid={token["uid"]}&version={version}', headers=await __headers__(token), data={}))
        # print(r1)
        for i in r1['farm']:
            # print(i)
            material = i['material']
            id_new = i['id']
            if not material:
                if id_new < 5:
                    # order_all = [1002, 1005, 1009, 1011, 1012, 1013, 1004, 1006, 1008, 1014]
                    order = random.choice(order_all)
                    await farm_material(token, len_new, id_new, order)
                    # order = random.randint(1002, 1015)
                    # await farm_material(token, len_new, id_new, order)

    except Exception as e:
        print(e)


# 查看是否能做饭
async def farm_products(token):
    global id_cl, dd
    try:
        b2 = {"next": "0"}
        r2 = json.loads(await post_json(f'{host}/v8/api/farm/product?uid={token["uid"]}&version={version}',headers=await __headers__(token), data=b2))
        # print(r2)
        for q in list(reversed(r2['items'])):
            # print(q)
            status = q['status']
            id_cl = q['id']
            if status > 0:
                compound = q['compound']
                dd = 0
                for o in q['compound']:
                    holdNum = o['holdNum']
                    consume = o['consume']
                    if holdNum >= consume:
                        dd += 1
                if len(compound) == dd:
                    return True, id_cl



    except Exception as e:
        print(e)


# 空闲做饭
async def farm_kitchen_detail(token, len_new):
    try:
        r1 = json.loads(await post_json(f'{host}/v8/api/farm/kitchen/detail?uid={token["uid"]}&version={version}',headers=await __headers__(token), data={}))
        # print(r1)
        for i in r1['kitchen']:
            # print(i)
            zt_id = i['id']
            if i['product']:
                seconds = i['product']['seconds']
                # print(seconds)
                if seconds < 1:
                    await farm_product_harvest(token, len_new, zt_id)
                print(f"[账号{len_new + 1}]厨房做饭->正在做{i['product']['name']}")
            else:
                orzf, id_cls = await farm_products(token)
                if orzf:
                    await farm_product_make(token, len_new, zt_id, id_cls)

    except Exception as e:
        print(e)


# 收菜
async def farm_product_harvest(token, len_new, zt_id):
    try:
        body = {"estateId":str(zt_id)}
        r1 = json.loads(await post_json(f'{host}/v8/api/farm/product/harvest?uid={token["uid"]}&version={version}',headers=await __headers__(token), data=body))
        # print(r1)
        print(f"[账号{len_new + 1}]厨房做饭->{zt_id}号灶台{r1['product'][0]['name']}熟了")
    except Exception as e:
        print(e)


# 直接做饭
async def farm_product_make(token, len_new, order, id_cls):
    try:
        body = {"estateId":str(order),"productId":str(id_cls),"productNum":"1"}
        r1 = json.loads(await post_json(f'{host}/v8/api/farm/product/make?uid={token["uid"]}&version={version}',headers=await __headers__(token), data=body))
        # print(r1)
        print(f"[账号{len_new + 1}]厨房做饭->{order}灶台做饭成功丨ID:{id_cls}丨{r1['seconds'] / 60}分钟成熟")
    except Exception as e:
        print(e)


# 收获农作物
async def farm_data_seconds(token, len_new):
    try:
        seconds_all = 0
        r1 = json.loads(await post_json(f'{host}/v8/api/farm/data?uid={token["uid"]}&version={version}',headers=await __headers__(token), data={}))
        # print(r1)
        for i in r1['farm']:
            material = i['material']
            if material:
                if i['material']['seconds'] > 900:
                    seconds_all += 1
        print(f"[账号{len_new + 1}]植物加速->共有{seconds_all}个超过15分钟才得收获")
        if seconds_all > 4:
            await farm_product_speed(token, len_new)
    except Exception as e:
        print(e)


# 酒楼
async def farm_drink(token, len_new):
    try:
        b1 = {"next":"0"}
        r1 = json.loads(await post_json(f'{host}/v8/api/farm/tavern?uid={token["uid"]}&version={version}',headers=await __headers__(token), data=b1))
        # print(r1)
        if r1['next']:
            for i in r1['items']:
                price = i['price']
                holdNum = i['holdNum']
                name = i['name']
                goodsId = i['id']
                print(f"[账号{len_new + 1}]酒楼求购->{goodsId}丨{name}丨价格:{price}丨持有:{holdNum}")
                if name == '芦笋烧肉' and float(price) > 9.9 and float(holdNum) > 0:
                    await farm_drink_sell(token, len_new, goodsId, name, price, holdNum)
                if name == '水果拼盘' and float(price) > 9.9 and float(holdNum) > 0:
                    await farm_drink_sell(token, len_new, goodsId, name, price, holdNum)
                if name == '竹笋豆腐' and float(price) > 9.9 and float(holdNum) > 0:
                    await farm_drink_sell(token, len_new, goodsId, name, price, holdNum)
                if name == '什锦丸子' and float(price) > 9.9 and float(holdNum) > 0:
                    await farm_drink_sell(token, len_new, goodsId, name, price, holdNum)
                if name == '东坡肉' and float(price) > 9 and float(holdNum) > 0:
                    await farm_drink_sell(token, len_new, goodsId, name, price, holdNum)
                if name == '水果炖奶' and float(price) > 8.9 and float(holdNum) > 0:
                    await farm_drink_sell(token, len_new, goodsId, name, price, holdNum)
                if name == '牛奶糕' and float(price) > 8 and float(holdNum) > 0:
                    await farm_drink_sell(token, len_new, goodsId, name, price, holdNum)
                if name == '药膳汤' and float(price) > 8 and float(holdNum) > 0:
                    await farm_drink_sell(token, len_new, goodsId, name, price, holdNum)
                if name == '鸡蛋灌饼' and float(price) > 8 and float(holdNum) > 0:
                    await farm_drink_sell(token, len_new, goodsId, name, price, holdNum)
                if name == '小笼包' and float(price) > 8 and float(holdNum) > 0:
                    await farm_drink_sell(token, len_new, goodsId, name, price, holdNum)
                if name == '绿茶豆腐' and float(price) > 8 and float(holdNum) > 0:
                    await farm_drink_sell(token, len_new, goodsId, name, price, holdNum)
                if name == '番茄炒蛋' and float(price) > 8 and float(holdNum) > 0:
                    await farm_drink_sell(token, len_new, goodsId, name, price, holdNum)
                if name == '拍黄瓜' and float(price) > 8 and float(holdNum) > 0:
                    await farm_drink_sell(token, len_new, goodsId, name, price, holdNum)
                if name == '炒时蔬' and float(price) > 8 and float(holdNum) > 0:
                    await farm_drink_sell(token, len_new, goodsId, name, price, holdNum)
                if name == '馒头' and float(price) > 8 and float(holdNum) > 0:
                    await farm_drink_sell(token, len_new, goodsId, name, price, holdNum)

    except Exception as e:
        print(e)


# 出售给酒楼
async def farm_drink_sell(token, len_new, goodsId, name, price, holdNum):
    try:
        t = {"sellDetail": f"[{{\"goodsId\":\"{goodsId}\",\"goodsNum\":{holdNum}}}]"}
        b1 = json.dumps(t)
        r1 = json.loads(await post_json(f'{host}/v8/api/farm/tavern/sell?uid={token["uid"]}&version={version}',headers=await __headers__(token), data=b1))
        print(r1)
        print(f"[账号{len_new + 1}]酒楼出售->{name}出售成功丨价格:{price}")
    except Exception as e:
        print(e)


# 铜钱商店
async def farm_copper_shop(token, len_new):
    try:
        r1 = json.loads(await post_json(f'{host}/v8/api/farm/shop?uid={token["uid"]}&version={version}', headers=await __headers__(token), data={}))
        # print(r1)
        copperNum = r1['copperNum']         # 铜钱
        stoneNum = r1['stoneNum']           # 灵石
        print(f"[账号{len_new + 1}]集市商店->铜钱:{copperNum}丨灵石:{stoneNum}")
        # if 'stoneItems' in r1:
        #     for i in r1['stoneItems']:
        #         # print(i)
        #         name = i['name']
        #         goodsId = i['id']
        #         if name == '悟性丹' or name == '资质丹':
        #             if float(i['stock']) > 0 and float(i['price']) < float(stoneNum):
        #                 await farm_shop_buy(token, len_new, goodsId, 2)
        if 'copperItems' in r1:
            for i in r1['copperItems']:
                name = i['name']
                goodsId = i['id']
                if name == '悟性丹' or name == '资质丹':
                    if float(i['stock']) > 0 and float(i['price']) < float(copperNum):
                        await farm_shop_buy(token, len_new, goodsId, 1)
    except Exception as e:
        print(e)


# 购买丹药
async def farm_shop_buy(token, len_new, goodsId, currencyType):
    try:
        b1 = {"goodsId":goodsId,"goodsNum":"1","currencyType": currencyType}
        r1 = json.loads(await post_json(f'{host}/v8/api/farm/shop/buy?uid={token["uid"]}&version={version}', headers=await __headers__(token), data=b1))
        # print(r1)
        if 'errorCode' in r1:
            if r1['errorCode'] == 400:
                return 1
        name = r1['name']
        print(f"[账号{len_new + 1}]购买丹药->ID:{goodsId}丨{name}")
    except Exception as e:
        print(e)

# 仓库
async def farm_stash(token, len_new):
    try:
        b1 = {"next":"0","type":"1"}
        r1 = json.loads(
            await post_json(f'{host}/v8/api/farm/warehouse?uid={token["uid"]}&version={version}', headers=await __headers__(token), data=b1))
        for _ in r1['items']:
            plant_id = _['id']
            name = _['name']
            num = _['num']
            print(f"[账号{len_new + 1}]仓库->ID:{plant_id}丨名称:{name}丨数量:{num}")
            if num > 88:
                materialNum = num - 88
                await farm_sell_plant(token, len_new, name, plant_id, materialNum)
                await asyncio.sleep(random.randint(5, 9))
    except Exception as e:
        print(e)

# 卖植物
async def farm_sell_plant(token, len_new, name, materialId, materialNum):
    try:
        b1 = {"materialId": materialId, "materialNum": materialNum}
        r1 = json.loads(await post_json(f'{host}/v8/api/farm/material/sell?uid={token["uid"]}&version={version}',headers=await __headers__(token), data=b1))
        await post_json(f'{host}/v8/api/farm/finance?uid={token["uid"]}&version={version}',headers=await __headers__(token), data={})
        print(f"[账号{len_new + 1}]卖植物->{name}卖出成功丨数量:{materialNum}丨获得:{r1['copperNum']}铜币")
    except Exception as e:
        print(e)



###################################################################################################################################################

# 加速农作物
async def farm_product_speed(token, len_new):
    try:
        time_new = str(int(time.time() * 1000))
        body = {"sceneId":"1","hasRootRights":"0","imei":"","androidId":token['oaid'],"oaid":"","appVersion":version,"os":"android","osVersion":"Android 9","device":"MI 6","network":"4g","pixel":"1080*1920","clientTime":time_new}
        r1 = json.loads(await post_json(f'{host}/v8/api/farm/ad/info?uid={token["uid"]}&version={version}',headers=await __headers__(token), data=body))
        # print(r1)
        if float(r1['adNum']) > 0:
            credential = r1['credential']
            adId = r1['items'][2]['adId']
            await asyncio.sleep(random.randint(16,25))
            await farm_ad_info(token, len_new, adId, credential)
        else:
            print(f"[账号{len_new + 1}]植物加速->没有广告次数")
    except Exception as e:
        print(e)


# 获取广告ID
async def farm_ad_info(token, len_new, adId, credential):
    try:
        body = {"adId":adId,"sceneId":"1","eventId":"1","credential":credential}
        r1 = json.loads(await post_json(f'{host}/v8/api/farm/ad/report?uid={token["uid"]}&version={version}',headers=await __headers__(token), data=body))
        # print(r1)
        if 'adLogId' in r1:
            adLogId = r1['adLogId']
            await asyncio.sleep(random.randint(2, 4))
            await farm_material_accelerate(token, len_new, adLogId)
        # print(f"[账号{len_new + 1}]加速农作物->{zt_id}号灶台{r1['product'][0]['name']}熟了")
    except Exception as e:
        print(e)


# 提交广告
async def farm_material_accelerate(token, len_new, adLogId):
    try:
        body = {"adLogId": adLogId}
        r1 = json.loads(await post_json(f'{host}/v8/api/farm/material/accelerate?uid={token["uid"]}&version={version}',headers=await __headers__(token), data=body))
        # print(r1)
        if 'seconds' in r1:
            print(f"[账号{len_new + 1}]植物加速->共加速{r1['seconds']}s")
    except Exception as e:
        print(e)




public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuDy6NwJNnTHfntnQCSPlgLUdXCKtZR3yJVbGGoWn718WE4xs77bFMBnvdXAs3K6Z46s6HH6niREJJDSTB0N9GblOjaTXTl8yMO4Lc6xOyaN2krDu/WbnIToMByn6sxOfhYu2URjSqJe+9lQqPylliXb66LEqCdxUl0vBR3/j+/109jjkPMoQ+Xhr61MPHB8qK1r6Dr2clMbftV9/Pvs/FzILoCLYYVj4atQcYn08Yzw0+2UbHXoVTulcwwkYo67Js2yEiJtUeLECLMtdJSJan4sLk8Y54ivb52JpgvB5lDzb9bjIFzOdZe7JpOrcIZoVJeErpjcKzgMK/xsifQECKwIDAQAB"

def public_key_decrypt(rsa_public_key_der_b64, qr_code_cipher_b64):
    try:
        missing_padding = len(qr_code_cipher_b64) % 4
        if missing_padding != 0:
            qr_code_cipher_b64 += '=' * (4 - missing_padding)
        qr_code_cipher = base64.b64decode(qr_code_cipher_b64)
        public_keys = base64.b64decode(rsa_public_key_der_b64)
        rsa_public_key = PublicKey.load_pkcs1_openssl_der(public_keys)
        cipher_text_bytes = transform.bytes2int(qr_code_cipher)
        decrypted_text = core.decrypt_int(cipher_text_bytes, rsa_public_key.e, rsa_public_key.n)
        final_text = transform.int2bytes(decrypted_text)
        final_qr_code = final_text[final_text.index(0) + 1:]
        return final_qr_code.decode()
    except Exception as ex:
        print(f"RSA解密发生错误：{ex}")
        return None


def public_key_encrypt(rsa_public_key_64, text):
    try:
        rsa_public_key = base64.b64decode(rsa_public_key_64)
        pubkey = PublicKey.load_pkcs1_openssl_der(rsa_public_key)
        encrypt_text = rsa.encrypt(base64.b64decode(text.encode()), pubkey)
        return base64.b64encode(encrypt_text).decode()
    except Exception as e:
        print(f"RSA加密发生错误：{e}")
        return None


def add_to_16(value):
    while len(value) % 16 != 0:
        value += '\0'
    return str.encode(value)


def get_key(n):
    c_length = int(n)
    source = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    length = len(source) - 1
    result = ''
    for i in range(c_length):
        result += source[random.randint(0, length)]
    return result


def getRandomKey(n):
    charstr = '0123456789abcdef'
    key = ''
    for i in range(n * 2):
        key += charstr[random.randint(0, len(charstr) - 1)]
    return base64.b64encode(bytes.fromhex(key)).decode()


def aes_decrypt(aesKey, aesIv, text):
    aes = AES.new(add_to_16(aesKey), AES.MODE_CBC, add_to_16(aesIv))
    base64_decrypted = base64.decodebytes(text.encode(encoding='utf-8'))
    decrypted_text = re.compile('[\\x00-\\x08\\x0b-\\x0c\\x0e-\\x1f\n\r\t]').sub('', aes.decrypt(base64_decrypted).decode())
    return decrypted_text


def aes_encrypt(aesKey, aesIv, text):
    __BLOCK_SIZE_16 = AES.block_size
    cipher = AES.new(base64.b64decode(aesKey.encode()), AES.MODE_CBC, aesIv.encode())  # 初始化加密器
    x = __BLOCK_SIZE_16 - (len(text) % __BLOCK_SIZE_16)
    if x != 0:
        text = text + chr(x) * x
    encrypt_aes = cipher.encrypt(text.encode('utf-8'))
    encrypted_text = base64.encodebytes(encrypt_aes).decode()
    return encrypted_text


def requ_encrypt(body):
    try:
        aesKey = getRandomKey(32)
        aesIv = get_key(16)
        rsa_encrypt = public_key_encrypt(public_key, aesKey)
        aes_encrypt_text = aes_encrypt(aesKey, aesIv, body)
        request_body = rsa_encrypt + '.' + aesIv + aes_encrypt_text
        return request_body
    except Exception as e:
        print(f"Request加密失败：{e}")
        return None


def resp_decrypt(response):
    try:
        if '.' not in response:
            return response
        resp_before = response.split('.')[0]
        resp_after = response.split('.')[1]
        aeskey = public_key_decrypt(public_key, resp_before)
        if aeskey is not None:
            aesiv = resp_after[:16]
            encrypt_text = resp_after[16:]
            resp = aes_decrypt(aeskey, aesiv, encrypt_text)
            return resp
        else:
            return None
    except Exception as e:
        print(f"response加密失败：{e}")

# 异步请求
async def post_json(url, headers, data):
    async with aiohttp.ClientSession() as session:
        async with session.post(url=url, headers=headers, data=data, ssl=False) as resp:
            return await resp.text()


# 异步请求
async def get_json(url, headers):
    async with aiohttp.ClientSession() as session:
        async with session.get(url=url, headers=headers, ssl=False) as resp:
            return await resp.json()


async def start(ce_token_new):
    try:
        print(f"==========共找到{len(ce_token_new)}个账号==========")
        await asyncio.gather(*[asyncio.create_task(user_info(i, ce_token_new.index(i))) for i in ce_token_new])
        print(f"------------------收获植物动物------------------")
        await asyncio.gather(*[asyncio.create_task(farm_data(i, ce_token_new.index(i))) for i in ce_token_new])
        print(f"------------------种田------------------")
        await asyncio.gather(*[asyncio.create_task(farm_kitchen(i, ce_token_new.index(i))) for i in ce_token_new])
        print(f"------------------做饭------------------")
        await asyncio.gather(*[asyncio.create_task(farm_kitchen_detail(i, ce_token_new.index(i))) for i in ce_token_new])
        print(f"------------------加速农作物------------------")
        await asyncio.gather(*[asyncio.create_task(farm_data_seconds(i, ce_token_new.index(i))) for i in ce_token_new])
        print(f"------------------铜钱商店------------------")
        await asyncio.gather(*[asyncio.create_task(farm_copper_shop(i, ce_token_new.index(i))) for i in ce_token_new])
        if int(datetime.datetime.now().time().minute) > 55:
            print(f"------------------酒楼------------------")
            await asyncio.gather(*[asyncio.create_task(farm_drink(i, ce_token_new.index(i))) for i in ce_token_new])
        if int(datetime.datetime.now().time().minute) > 55:
            print(f"------------------仓库------------------")
            await asyncio.gather(*[asyncio.create_task(farm_stash(i, ce_token_new.index(i))) for i in ce_token_new])
    except Exception as e:
        print(e)



if __name__ == '__main__':
    try:
        ce_token_new = json.load(open("Farming_immortality.json", 'r'))['data']
        asyncio.run(start(ce_token_new))
    except Exception as e:
        print(e)


