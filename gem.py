
# -*- coding:utf-8 -*-
"""
@Created on: 2024/9/4 23:18
@Author: zzzl
@Des；每日沙粒获取
"""
import asyncio
import json
import random
import aiohttp

"""
@ 下载链接微信打开  http://w.jqsjgwb.cn/wx/s?_co=797679&_st=tideNx&_v=v15
@ 账号配置在 Farming_immortality.json    文件里面
@ 版本 1.4
@ cron: 10 0,2 0,23 * * *
@ new Env('每日沙粒获取')
"""

################################################配置区域#########################################################

################################################配置区域#########################################################
################################################下面的不要动#########################################################
version = '1.1.0'  # 宝石版本
host = 'https://android-api.lucklyworld.com'



class Gem:
    def __init__(self, token, len_new):
        ...
        self.token = token
        self.len_new = len_new
        self.innerId = json.load(open("Gem_data.json", 'r'))['unified_data']['innerId']


    async def __headers__(self):
        try:
            headers = {
                'User-Agent': f'com.yiqun.gem/{version}-official Dalvik/2.1.0 (Linux; U; Android 12; 2201122C Build/SKQ1.211006.001)',
                'channel': 'official',
                'deviceId': self.token['DEVICEID'],
                'androidId': self.token['ANDROIDID'],
                'oaid': self.token['oaid'],
                'uid': self.token['uid'],
                'token': self.token['token'],
                'Host': 'android-api.lucklyworld.com',
            }
            return headers
        except Exception as e:
            print(e)

    # 账号信息
    async def user_info(self):
        try:
            resp = await post_json(f'{host}/v15/api/me?uid={self.token["uid"]}&version={version}', headers=await self.__headers__(), data={})
            # print(resp)
            r2 = await post_json(f'{host}/v15/api/gravel/trade/index?uid={self.token["uid"]}&version={version}', headers=await self.__headers__(), data={})
            # print(r2)
            if 'errorCode' in resp:
                print(f'[账号{self.len_new + 1}]信息->{resp.get("message")}')
                return False
            if 'userId' in resp:
                userId = resp['userId']  # id
                di = str(userId)[0:2] + '**' + str(userId)[4:]
                nickname = resp['nickname'][:4]  # 名字
                shells = r2.get('shells')    # 铜钱
                quantity = resp.get('rocks').get('quantity')  # 宝石
                print(f"[账号{self.len_new + 1}]信息->ID:{di}丨昵称:{nickname}丨沙粒:{shells}丨宝石:{quantity}")
            return True
        except Exception as e:
            print(e)

    # 沙粒余额
    async def gravel_balance(self):
        try:
            resp = await post_json(f'{host}/v15/api/gravel/trade/index?uid={self.token["uid"]}&version={version}', headers=await self.__headers__(), data={})
            if resp.get('shells'):
                return resp.get('shells')
            return 0
        except Exception as e:
            print(e)

    # 查询沙粒最低上架价格
    async def gravel_lowest_price(self):
        try:
            body = {
                'page': '1'
            }
            resp = await post_json(f'{host}/v15/api/gravel/trade/sale/list?uid={self.token["uid"]}&version={version}', headers=await self.__headers__(), data=body)
            # print(resp)
            if resp.get('list'):
                price = resp['list'][5]['price']
                # print(f'[账号{self.len_new + 1}]沙粒->第6个上架价格:{price}')
                return price
            return 1
        except Exception as e:
            print(e)

    # 上架沙粒
    async def shelf_sand(self):
        try:
            price = await self.gravel_lowest_price()
            await asyncio.sleep(random.randint(1, 3))
            await self.take_away_sand(price)
            # print(price, quantity)
            quantity = int(float(await self.gravel_balance()))
            await asyncio.sleep(random.randint(1, 3))
            if float(quantity) > 1:
                body = {
                    'price': price,
                    'quantity': quantity,
                }
                resp = await post_json(f'{host}/v15/api/gravel/trade/sale/post?uid={self.token["uid"]}&version={version}', headers=await self.__headers__(), data=body)
                # print(resp)
                if resp.get('message') == 'OK':
                    print(f'[账号{self.len_new + 1}]上架沙粒->上架成功')
                if resp.get('errorCode') == 40003:
                    print(f'[账号{self.len_new + 1}]上架沙粒->上架失败:{resp.get("message")}')
                if resp.get('errorCode') == 400:
                    print(f'[账号{self.len_new + 1}]上架沙粒->{resp.get("message")}')
            else:
                print(f'[账号{self.len_new + 1}]上架沙粒->沙粒太少再去攒攒吧')
        except Exception as e:
            print(e)

    # 下架沙粒
    async def take_away_sand(self, price):
        try:
            body = {
                'page': '1'
            }
            resp = await post_json(f'{host}/v15/api/gravel/trade/sale/orders?uid={self.token["uid"]}&version={version}', headers=await self.__headers__(), data=body)
            # print(resp)
            if resp.get('list'):
                await asyncio.sleep(random.randint(1, 3))
                for _ in resp['list']:
                    tradeId = _['tradeId']
                    state = _['state']
                    price_interior = _['price']
                    if state == 1 and price_interior > price:
                        # print(price_interior, price)
                        body = {
                            'tradeId': tradeId,
                        }
                        resp = await post_json(f'{host}/v15/api/gravel/trade/cancel?uid={self.token["uid"]}&version={version}', headers=await self.__headers__(), data=body)
                        # print(resp)
                        if resp.get('message') == 'OK':
                            print(f'[账号{self.len_new + 1}]下架沙粒->下架成功')

        except Exception as e:
            print(e)

    # 查最近获得宝石记录
    async def recent_obtain_gem(self):
        try:
            jewel = await self.gem_balance()
            if jewel > 0:
                buyerInnerId = json.load(open("Gem_data.json", 'r'))['unified_data']['buyerInnerId']
                await asyncio.sleep(random.randint(10, 25) / 9)
                await self.query_id_info(buyerInnerId)
                await asyncio.sleep(random.randint(10, 25) / 9)
                if float(self.innerId) > float(jewel):
                    print(f'[账号{self.len_new + 1}]赠送宝石->宝石数量未达到设置值，不赠送')
                    return False
                if self.token['uid'] == buyerInnerId:
                    print(f'[账号{self.len_new + 1}]赠送宝石->赠送ID是自己，不赠送')
                    return False
                body = {
                    'receivedUserId': buyerInnerId,
                    'quantity': jewel
                }
                resp = await post_json(f'{host}/v15/api/rocks/transfer?uid={self.token["uid"]}&version={version}', headers=await self.__headers__(), data=body)
                # print(resp)
                if resp.get('message'):
                    print(f'[账号{self.len_new + 1}]赠送宝石->{buyerInnerId}丨数量:{jewel}   🎉')
            else:
                print(f'[账号{self.len_new + 1}]赠送宝石->宝石不足1')
        except Exception as e:
            ...
            # print(e)

    # 获取宝石余额
    async def gem_balance(self):
        try:
            resp = await post_json(f'{host}/v15/api/rocks/transfer/info?uid={self.token["uid"]}&version={version}', headers=await self.__headers__(), data={})
            # print(resp)
            if 'rocks' in resp:
                rocks = resp.get('rocks')
                transferFeeRate = resp.get('transferFeeRate')
                jewel = int(float(rocks) * (1.00 - float(transferFeeRate)))
                return jewel
            return 0
        except Exception as e:
            print(e)

    # 查询ID详情
    async def query_id_info(self, buyerInnerId):
        try:
            await post_json(f'{host}/api/user/info?uid={self.token["uid"]}&version={version}', headers=await self.__headers__(), data={'userId': buyerInnerId})
            # print(resp)
        except Exception as e:
            print(e)










# 异步请求
async def post_json(url, headers, data):
    async with aiohttp.ClientSession() as session:
        async with session.post(url=url, headers=headers, data=data, ssl=False) as resp:
            return await resp.json()


# 异步请求
async def get_json(url, headers):
    async with aiohttp.ClientSession() as session:
        async with session.get(url=url, headers=headers, ssl=False) as resp:
            return await resp.json()


async def start(ce_token_new):
    try:
        print(f"==========共找到{len(ce_token_new)}个账号==========")
        print(f"------------------账号信息------------------")
        await asyncio.gather(*[asyncio.create_task(Gem(i, ce_token_new.index(i)).user_info()) for i in ce_token_new])
        print(f"------------------上架沙粒------------------")
        await asyncio.gather(*[asyncio.create_task(Gem(i, ce_token_new.index(i)).shelf_sand()) for i in ce_token_new])
        print(f"------------------赠送宝石------------------")
        await asyncio.gather(*[asyncio.create_task(Gem(i, ce_token_new.index(i)).recent_obtain_gem()) for i in ce_token_new])
    except Exception as e:
        print(e)



if __name__ == '__main__':
    try:
        ce_token_new = json.load(open("Gem_data.json", 'r', encoding='utf-8'))['data']
        asyncio.run(start(ce_token_new))
    except Exception as e:
        print(e)


