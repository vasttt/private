# -*- coding:utf-8 -*-
"""
@Created on: 2024/4/6 20:58
@Author: zzzl
@Des；星抖短剧
"""
import json
import os
import random
import time
import datetime
import requests
from hashlib import md5
from version_conf import VC

"""
@ 下载地址  https://www.xingdouduanju.com/pages/register/index.html?invite_code=629709
@ cron: 5 0,9-23 * * *
@ new Env('星抖短剧')
@ 版本  2.1
"""

##################################下面不要动##################################
host = 'https://api.xingdouduanju.com'
Current_version = '2.1'

# 执行次数
def task_len(fun):
    def wrapper(*args, **kwargs):
        for i in range(2):
            fun(*args, **kwargs)
        return 0

    return wrapper


class Star_trembling:
    def __init__(self, token, len_new):
        self.Authorization = token['Authorization']
        self.len_new = len_new
        self.headers = {
            'Authorization': self.Authorization,
            'X-Version-Code': '130',
            'X-Platform': 'android',
            'X-System': '12',
            'X-Brand': 'Xiaomi',
            'X-Device-ID': 'BC5B12E76B819898',
            'X-Client-Id': 'com.xingdou.app',
            'distributor-key': 'xingdou',
            'Host': 'api.xingdouduanju.com',
            'Connection': "Keep-Alive",
            'Accept-Encoding': "gzip",
            'Content-Type': "application/json",
            'User-Agent': "okhttp/4.8.1",
        }
    # 信息
    def info(self):
        r1 = requests.request('get', f'{host}/api/user?include=VipLevel', headers=self.headers).json()
        r2 = requests.request('get', f'{host}/api/gold_pigs/info', headers=self.headers).json()
        # print(r2)
        if 'code' in r1:
            if r1['code'] == 200001:
                nickname = r1['data']['nickname']
                goldPigCount = r2['data']['goldPigCount']
                print(
                    f"[账号{self.len_new + 1}]信息->昵称:{nickname}丨金猪:{goldPigCount}丨金块:{r2['data']['walletBullion']['balance']}丨金币:{r2['data']['walletGold']['balance']}")
    # 观看广告
    def complete(self, task_id):

        timestamp = str(int(time.time() * 1000))
        nonces = md5(timestamp.encode()).hexdigest()
        sign = md5(f"{timestamp}&{task_id}&{nonces}&kjKjb8WRmfb77U6IMqsVtIuIFQCvab4JBqABNqSp&true".encode()).hexdigest()
        body = json.dumps({"timestamp": timestamp, "nonce": nonces, "id": str(task_id), 'done': True, "sign": sign})
        r1 = requests.request('post', f'{host}/api/gold_tasks/{task_id}/complete', data=body, headers=self.headers).json()
        if 'success' in r1:
            if r1['success']:
                print(f'[账号{self.len_new + 1}]观看->{r1["success"]}丨获得{r1["data"]["reward"]}金币')
            else:
                print(f'[账号{self.len_new + 1}]观看->{r1["message"]}')
        time.sleep(random.randint(33, 44))

    def complete_new(self, task_id):
        timestamp = str(int(time.time() * 1000))
        nonces = md5(timestamp.encode()).hexdigest()
        sign = md5(f"{task_id}&{timestamp}&{nonces}&kjKjb8WRmfb77U6IMqsVtIuIFQCvab4JBqABNqSp&true".encode()).hexdigest()
        body = json.dumps({"timestamp": timestamp, "nonce": nonces, "id": str(task_id), 'done': True, "sign": sign})
        r1 = requests.request('post', f'{host}/api/gold_tasks/{task_id}/complete', data=body, headers=self.headers).json()
        if 'success' in r1:
            if r1['success']:
                print(f'[账号{self.len_new + 1}]观看->{r1["success"]}丨获得{r1["data"]["reward"]}🧧')
            else:
                print(f'[账号{self.len_new + 1}]观看->{r1["message"]}')
        time.sleep(random.randint(33, 44))
    # 任务列表
    def gold_tasks(self):
        r1 = requests.request('get', f'{host}/api/gold_tasks', headers=self.headers).json()
        # print(r1)
        if 'code' in r1:
            if r1['code'] == 200001:
                for i in r1['data']['tasks']:
                    task_id = i['id']
                    name = i['name']
                    if task_id == 5:
                        times = i['times']
                        completedCount = i['completedCount']
                        print(f'[账号{self.len_new + 1}]任务->{name}丨已完成{completedCount}次')
                        if completedCount < times:
                            self.complete(task_id)
                    if task_id == 6:
                        times = i['times']
                        completedCount = i['completedCount']
                        print(f'[账号{self.len_new + 1}]任务->{name}丨已完成{completedCount}次')
                        if completedCount < times:
                            self.complete(task_id)
                    if task_id == 11:
                        times = i['times']
                        completedCount = i['completedCount']
                        print(f'[账号{self.len_new + 1}]任务->{name}丨已完成{completedCount}次')
                        if completedCount < times:
                            self.complete_new(task_id)

    # 购买野猪
    def gold_pigs(self):
        r1 = requests.request('get', f'{host}/api/gold_pigs/info', headers=self.headers).json()
        if int(r1['data']['goldPigCount']) < 2998:
            response = requests.request('get', f'{host}/api/gold_pigs/info', headers=self.headers).json()
            if float(response["data"]["walletGold"]["balance"]) > float(response["data"]["price"]):
                # go = int(float(response["data"]["walletGold"]["balance"]) // float(response["data"]["price"]))
                self.gold_exchange(2)
        else:
            print(f'[账号{self.len_new + 1}]购买野猪->金猪已满')

    # 购买野猪
    def gold_exchange(self, go):
        for _ in range(go):
            timestamp = str(int(time.time() * 1000))
            nonces = md5(timestamp.encode()).hexdigest()
            sign = md5(f"{timestamp}&{nonces}&kjKjb8WRmfb77U6IMqsVtIuIFQCvab4JBqABNqSp".encode()).hexdigest()
            payload = json.dumps({
                "nonce": nonces,
                "timestamp": timestamp,
                "sign": sign
            })
            response = requests.request('post', f'{host}/api/gold_pigs/gold_exchange', data=payload, headers=self.headers).json()
            if "OK" in response["message"]:
                print(f"[账号{self.len_new + 1}]购买野猪丨{response['message']}")
            else:
                print(f"[账号{self.len_new + 1}]购买野猪->{response['message']}")
                exit(f"{response['message']}")

    # 收取金块
    def pickup_metals(self, pickup_id, bullions):
        timestamp = str(int(time.time() * 1000))
        nonces = md5(timestamp.encode()).hexdigest()
        sign = md5(f"{timestamp}&{pickup_id}&{nonces}&kjKjb8WRmfb77U6IMqsVtIuIFQCvab4JBqABNqSp".encode()).hexdigest()
        payload = json.dumps({
            "timestamp": timestamp,
            "nonce": nonces,
            "id": str(pickup_id),
            "sign": sign
        })
        r1 = requests.request('post', f'{host}/api/gold_pigs/{pickup_id}/collect_bullion', data=payload,
                              headers=self.headers).json()
        if 'success' in r1:
            if r1['success']:
                print(f"[账号{self.len_new + 1}]拾取金块->拾取金块:{bullions} 🎉")

    # 一键拾取
    def collect_all_bullion(self):
        # time.sleep(random.randint(6, 8))
        timestamp = str(int(time.time() * 1000))
        nonce = md5(timestamp.encode()).hexdigest()
        sign = md5(f"{nonce}&{timestamp}&false&kjKjb8WRmfb77U6IMqsVtIuIFQCvab4JBqABNqSp".encode()).hexdigest()
        payload = json.dumps({
          "timestamp": timestamp,
          "nonce": nonce,
          "hasWatchAd": False,
          "sign": sign
        })
        headers = self.headers
        headers['Content-Length'] = '136'
        headers['Content-Type'] = 'application/json; charset=utf-8'
        r1 = requests.request('post', f'{host}/api/gold_pigs/collect_all_bullion', data=payload, headers=headers).json()
        # print(r1)
        if 'success' in r1:
            if r1['success']:
                print(f"[账号{self.len_new + 1}]拾取金块->一键拾取金块:{r1['message']} 🎉")
            else:
                print(f"[账号{self.len_new + 1}]拾取金块->{r1['message']}")

    # 状态
    def gold_pigs_info(self):
        r2 = requests.request('get', f'{host}/api/gold_pigs/info', headers=self.headers).json()
        for _ in range((int(r2['data']['goldPigCount']) // 8) + 1):
            r1 = requests.request('get', f'{host}/api/gold_pigs/info', headers=self.headers).json()
            if 'waitCollectGoldPigs' in r1['data']:
                if r1['data']['waitCollectGoldPigs']:
                    r3 = requests.request('get', f'{host}/api/user?include=VipLevel', headers=self.headers).json()
                    if r3['data']['vipExpiryTime']:
                        r4 = requests.request('get', f'{host}/api/gold_tasks', headers=self.headers).json()
                        if r4['code'] == 200001:
                            for i in r4['data']['tasks']:
                                task_id = i['id']
                                finished = i['completedCount']
                                if task_id == 6:
                                    if finished:
                                        self.collect_all_bullion()
                                        return 1
                    for i in r1['data']['waitCollectGoldPigs']:
                        bullions = i['bullions']
                        pickup_id = i['id']
                        self.pickup_metals(pickup_id, bullions)
                        time.sleep(random.randint(10, 15) / 12)
                else:
                    break
            time.sleep(random.randint(10, 15) / 12)

    # 兑换金币
    def exchange(self):
        try:
            r2 = requests.request('get', f'{host}/api/bullion_goods?type=0', headers=self.headers).json()
            # print(r2)
            r3 = requests.request('get', f'{host}/api/gold_pigs/info', headers=self.headers).json()
            if float(r3['data']['walletBullion']['balance']) > 1000:
                exchange_id = r2['data'][0]['id']
                timestamp = str(int(time.time() * 1000))
                nonces = md5(timestamp.encode()).hexdigest()
                sign = md5(f"{timestamp}&{exchange_id}&{nonces}&kjKjb8WRmfb77U6IMqsVtIuIFQCvab4JBqABNqSp".encode()).hexdigest()
                payload = json.dumps({
                    "timestamp": timestamp,
                    "nonce": nonces,
                    "id": str(exchange_id),
                    "sign": sign
                })
                r1 = requests.request('post', f'{host}/api/bullion_goods/1774006569529245696/exchange', data=payload, headers=self.headers).json()
                # print(r1)
                if 'success' in r1:
                    if r1['success']:
                        print(f"[账号{self.len_new + 1}]兑换金币->成功  🎉")
                    else:
                        print(f"[账号{self.len_new + 1}]兑换金币->{r1}")
            else:
                print(f"[账号{self.len_new + 1}]兑换金币->金块不足1000")
        except:
            ...

    def vip_levels(self):
        try:
            r1 = requests.request('get', f'{host}/api/vip_levels', headers=self.headers).json()
            # print(r1)
            if 'data' in r1:
                for _ in r1['data']:
                    userStatusName = _['userStatusName']
                    hasReceivedDividend = _['hasReceivedDividend']
                    if userStatusName == '已开通':
                        print(f"[账号{self.len_new + 1}]会员->{_['name']}丨是否领取:{hasReceivedDividend}")
                        if _['canReceiveDividend']:
                            self.user_vip_dividends()
        except Exception as e:
            print(e)

    def user_vip_dividends(self):
        try:
            timestamp = str(int(time.time() * 1000))
            nonces = md5(timestamp.encode()).hexdigest()
            sign = md5(f"{timestamp}&{nonces}&kjKjb8WRmfb77U6IMqsVtIuIFQCvab4JBqABNqSp".encode()).hexdigest()
            payload = json.dumps({
                "timestamp": timestamp,
                "nonce": nonces,
                "sign": sign
            })
            r1 = requests.request('post', f'{host}/api/user_vip_dividends/receive', headers=self.headers, data=payload).json()
            # print(r1)
            if 'success' in r1:
                if r1['success']:
                    print(f"[账号{self.len_new + 1}]会员->{r1['message']}")
            else:
                print(r1)
        except Exception as e:
            print(e)


    #提现
    def withdraw(self):
        try:
            payeeId = "13211111816"
            r1 = requests.request('get', f'{host}/api/user/profile', headers=self.headers).json()
            # print(r1)
            if r1['success']:
                balance = r1['data']['wallet']['balance']
                if float(balance) > 20:
                    r3 = requests.request('get', f'{host}/api/withdraw_amounts', headers=self.headers).json()
                    if r3['success']:
                        # print(r3)
                        for _ in r3['data']['list']:
                            # print(_)
                            amount = _['amount']
                            if float(amount) < float(balance):
                                withdrawAmountId = _['id']
                                timestamp = str(int(time.time() * 1000))
                                nonces = md5(timestamp.encode()).hexdigest()
                                sign = md5(f"{payeeId}&{timestamp}&{withdrawAmountId}&{nonces}&kjKjb8WRmfb77U6IMqsVtIuIFQCvab4JBqABNqSp".encode()).hexdigest()
                                payload = json.dumps({
                                    "timestamp": timestamp,
                                    "nonce": nonces,
                                    "withdrawAmountId": withdrawAmountId,
                                    "payeeId": payeeId,
                                    "sign": sign
                                })
                                r2 = requests.request('post', f'{host}/api/withdraws', data=payload, headers=self.headers).json()
                                # print(r2)
                                if r2['code'] == 200001:
                                    print(f"[账号{self.len_new + 1}]提现->{r2['message']}")
                                else:
                                    print(f"[账号{self.len_new + 1}]提现->{r2['message']}")
        except Exception as e:
            print(e)


def start(ce_token_all):
    try:
        prompt = VC().get_version('星抖短剧')['prompt']
        Cloud_version = VC().get_version('星抖短剧')['version']
        if Cloud_version > Current_version:
            print(f"当前版本:{Current_version}  最新版本:{Cloud_version}")
            print(f'最新更新内容:{prompt}')
            print("老版本跑没问题就无需更新脚本")
        else:
            print(f"当前版本:{Current_version}  最新版本:{Cloud_version}")
            print(f'最新更新内容:{prompt}')
        print(f"==========共找到{len(ce_token_all)}个账号==========")
        print('----------------------------个心信息----------------------------')
        for i in ce_token_all:
            Star_trembling(i, ce_token_all.index(i)).info()
        print('----------------------------领取会员奖励----------------------------')
        for i in ce_token_all:
            Star_trembling(i, ce_token_all.index(i)).vip_levels()
        # 在十点到十八点之间运行
        if 10 <= datetime.datetime.now().hour < 18:
            print('----------------------------提现----------------------------')
            for i in ce_token_all:
                Star_trembling(i, ce_token_all.index(i)).withdraw()
        print('----------------------------广告任务----------------------------')
        for i in ce_token_all:
            Star_trembling(i, ce_token_all.index(i)).gold_tasks()
        print('----------------------------拾取金块----------------------------')
        for i in ce_token_all:
            Star_trembling(i, ce_token_all.index(i)).gold_pigs_info()
        print('----------------------------金块兑换金币----------------------------')
        for i in ce_token_all:
            Star_trembling(i, ce_token_all.index(i)).exchange()
        print('----------------------------买猪----------------------------')
        for i in ce_token_all:
            Star_trembling(i, ce_token_all.index(i)).gold_pigs()


    except:
        pass


def search_files():
    for root, dirs, files in os.walk(os.path.dirname(os.getcwd())):
        for file in files:
            if 'Star_trembling_data.json' in file:
                return os.path.join(root, file)


if __name__ == '__main__':
    try:
        ce_token_all = json.load(open(search_files(), 'r'))['data']
        random.shuffle(ce_token_all)
        start(ce_token_all)
    except Exception as e:
        print(e)
