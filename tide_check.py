# -*- coding:utf-8 -*-
"""
@Created on: 2024/8/25 9:58
@Author: zzzl
@Des；望潮阅读有礼
"""

import json
import os
import random
import time
import uuid
import requests
import hashlib
from datetime import datetime
from gmssl import sm2
from version_conf import VC

"""
@ cron: 15 9,23 * * *
@ new Env('望潮阅读有礼')
@ 版本  1.2
"""


#########################################下面不要动###################################################
host = 'https://vapp.taizhou.com.cn'
Current_version = '1.2'


def get_13_digit_timestamp():
    # 获取当前时间的时间戳（秒级）
    timestamp = time.time()
    # 将秒级时间戳转换为毫秒级（乘以1000）
    return int(timestamp * 1000)


def get_md5(text):
    m = hashlib.md5(text.encode(encoding='utf-8'))
    return m.hexdigest()


def sha256_sign(string):
    # 创建一个sha256哈希对象
    sha256_hash = hashlib.sha256()
    # 更新哈希对象的输入字符串
    sha256_hash.update(string.encode('utf-8'))
    # 获取哈希值的十六进制表示
    return sha256_hash.hexdigest()


def headers(id, account_id):
    timestamp = get_13_digit_timestamp()
    request_id = str(uuid.uuid4())  # 生成随机的UUID
    # request_id = '4cb18424-87a5-46bd-a8a2-7b0fe1cadf2c'
    input_string = f'/api/user_mumber/account_detail&&{id}&&{request_id}&&{timestamp}&&FR*r!isE5W&&64'
    signature = sha256_sign(input_string)
    headers = {
        'X-SESSION-ID': id,
        'X-REQUEST-ID': request_id,
        'X-TIMESTAMP': str(timestamp),
        'X-SIGNATURE': signature,
        'X-TENANT-ID': '64',
        'User-Agent': '6.0.2;00000000-648c-3742-ffff-ffffcc0ce5f4;Xiaomi 2201122C;Android;12;tencent;6.10.0',
        'X-ACCOUNT-ID': account_id,
        'Cache-Control': 'no-cache',
        'Host': 'vapp.taizhou.com.cn',
        'Connection': 'Keep-Alive',
        'Accept-Encoding': 'gzip'

    }
    return headers


class Tide_reading(object):
    def __init__(self, token, len_new):
        self.device_id = token['device_id']
        self.account_id = token['account_id']
        self.sessionId = token['sessionId']
        self.oneKey1 = 'djidfovnsdncv'
        self.ones2 = "sdbedjmvnsiv"
        self.len_new = len_new
        self.session = requests.session()
        self.session.headers = {
            'User-Agent': 'Mozilla/5.0 (Linux; Android 12; 2201122C Build/SKQ1.211006.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/127.0.6533.64 Mobile Safari/537.36;xsb_wangchao;xsb_wangchao;6.0.2;native_app;6.10.0', }

    def headers(self, url):
        timestamp = get_13_digit_timestamp()
        request_id = str(uuid.uuid4())  # 生成随机的UUID
        # request_id = '4cb18424-87a5-46bd-a8a2-7b0fe1cadf2c'
        input_string = f'{url}&&{self.sessionId}&&{request_id}&&{timestamp}&&FR*r!isE5W&&64'
        signature = sha256_sign(input_string)
        headers = {
            'X-SESSION-ID': self.sessionId,
            'X-REQUEST-ID': request_id,
            'X-TIMESTAMP': str(timestamp),
            'X-SIGNATURE': signature,
            'X-TENANT-ID': '64',
            'User-Agent': '6.0.2;00000000-648c-3742-ffff-ffffcc0ce5f4;Xiaomi 2201122C;Android;12;tencent;6.10.0',
            'X-ACCOUNT-ID': self.account_id,
            'Cache-Control': 'no-cache',
            'Host': 'vapp.taizhou.com.cn',
            'Connection': 'Keep-Alive',
            'Accept-Encoding': 'gzip'

        }
        return headers

    def info(self):
        try:
            resp = requests.get(f'{host}/api/user_mumber/account_detail',
                                headers=self.headers('/api/user_mumber/account_detail')).json()
            # print(resp)
            if 'code' in resp:
                if resp['code'] == 0:
                    nick_name = resp['data']['rst']['nick_name']
                    mobile = resp['data']['rst']['mobile']
                    print(f'[账号{self.len_new + 1}]信息->昵称:{nick_name}丨手机号:{mobile[-4:]}丨ID:{self.sessionId[-6:]}')
        except Exception as e:

            print(e)

    # 阅读任务

    def user_read_login(self):
        try:
            resp = self.session.get(f'https://xmt.taizhou.com.cn/prod-api/user-read/app/login?id={self.account_id}&sessionId={self.sessionId}&deviceId={self.device_id}').json()
            # print(resp)
            # print(self.session.)
            if 'code' in resp:
                if resp['code'] == 200:
                    needYz = resp.get('data').get('needYz')
                    if needYz:
                        ts = str(int(time.time() * 1000))
                        ms = '&&TlGFQAOlCIVxnKopQnW&&' + ts + '&&' + needYz
                        # XuGWzlcQIfLbnNBDieOVUKHSMypPRaAYrCtJjEogqmvxwFdksh = get_md5(ms)
                        r3 = self.session.get(
                            f'https://xmt.taizhou.com.cn/prod-api/user-read/yzmyz?signature=' + get_md5(
                                ms) + '&timestamp=' + ts + '&yzm=' + resp.get('data').get('yzm'))
                        print(r3)

                    r2 = self.session.get(f'https://xmt.taizhou.com.cn/prod-api/user-read/list/{str(time.strftime("%Y%m%d"))}').json()
                    # print(r2)
                    # print(r2.get('data').get('articleIsReadList'))
                    if 'code' in r2:
                        if r2['code'] == 200:
                            print('-' * 30)
                            print(f'[账号{self.len_new + 1}]阅读->进度:{r2.get("data").get("sum")}/{r2.get("data").get("completedCount")}')
                            for i in r2.get('data').get('articleIsReadList'):
                                isRead = i['isRead']
                                title = i['title']
                                newsId = i.get('newsId')
                                if not isRead:
                                    print('-' * 30)
                                    print(f'[账号{self.len_new + 1}]阅读->状态:{isRead}丨文章昵称:{title}')
                                    time.sleep(random.randint(2, 4))
                                    json_data = {'timestamp': get_13_digit_timestamp(), 'articleId': i.get('id'), 'accountId': self.account_id}
                                    r4 = self.session.get(f'https://xmt.taizhou.com.cn/prod-api/already-read/article/new?signature={self.get_CryptSM2(json_data)}').json()
                                    # print(r4)
                                    if 'code' in r4:
                                        if r4['code'] == 200:
                                            print(f'[账号{self.len_new + 1}]阅读->{r4["msg"]}丨{title}')
                                            time.sleep(random.randint(2, 4))
                                            self.article_detail(newsId)
                                    time.sleep(random.randint(16, 24))
                            # print('-' * 50)
                            # self.prize_draw()
                        else:
                            print(f'[账号{self.len_new + 1}]阅读->{r2["msg"]}')
                else:
                    print(f'[账号{self.len_new + 1}]阅读->{resp["msg"]}')
        except Exception as e:
            print(e)

    def article_detail(self, newsId):
        request_id = str(uuid.uuid4())
        time_new = str(int(time.time() * 1000))
        data = '/api/article/detail&&' + self.sessionId + '&&' + request_id + '&&' + time_new + '&&FR*r!isE5W&&64'
        SIGNATURE = hashlib.sha256(data.encode()).hexdigest()
        cccc = {
            "X-SESSION-ID": self.sessionId,
            "X-REQUEST-ID": request_id,
            "X-TIMESTAMP": time_new, "X-TENANT-ID": '64',
            "X-SIGNATURE": SIGNATURE}
        self.session.headers.update(cccc)
        r = requests.get(f'{host}/api/article/detail?id={str(newsId)}', headers=self.session.headers)
        # print(r.text)
        if 'success' in r.text:
            print(f'[账号{self.len_new + 1}]阅读->阅读成功！    🎉')


    def get_CryptSM2(self, datas):
        self.t1 = 'T^(QVWF]2VY%FCT&S&Y\\FXC^3&+[%W,NWJ!VQNDV!U&_^N-2]G (_"V^N]1P-T3BRV!%,.@WG_7&,PW%W7(B\'^"2JR&U!]ZF*APDR^,"_)O^D *!76]U$V+]4,BYER.]%%'
        data_txt = json.dumps(datas, separators=(',', ':'))
        sxdcc = self.rttt(self.t1)
        ertyhgf = sm2.CryptSM2(public_key=sxdcc, private_key=None, mode=1)
        data = ertyhgf.encrypt(data_txt.encode('utf-8')).hex()
        return data

    def rttt(self, input_string):
        self.jmKey = 'djidfovnsdncvsdbedjmvnsiv'
        tttiiik = (self.jmKey * (len(input_string) // len(self.jmKey))) + self.jmKey[:len(input_string) % len(self.jmKey)]
        rdddd = ''.join(chr(ord(x) ^ ord(y)) for x, y in zip(input_string,tttiiik))
        return rdddd


    # 查询抽奖
    def luck_draw(self):
        try:
            r1 = self.session.get(
                f'https://srv-app.taizhou.com.cn/tzrb/user/loginWC?accountId=' + self.account_id + '&sessionId=' + self.sessionId).json()
            # print(r1)
            # if 'code' in r1:
                # if r1['code'] == 200:
                    # print(f'[账号{self.len_new + 1}]抽奖->登录抽奖:{r1.get("message")}')
            self.session.headers.update({'Referer': 'https://srv-app.taizhou.com.cn/luckdraw/', 'Host': 'srv-app.taizhou.com.cn'})
            r2 = self.session.get('https://srv-app.taizhou.com.cn/tzrb/userAwardRecordUpgrade/pageList?pageSize=10&pageNum=1&activityId=67').json()
            # print(r2)

            if 'code' in r2:
                if r2['code'] == 200:
                    ...
                    createTime = r2.get('data')['records'][0].get('createTime')
                    date_object1 = datetime.strptime(createTime, "%Y-%m-%d %H:%M:%S").date()
                    date_object2 = datetime.now().date()
                    if date_object1 == date_object2:
                        print(f'[账号{self.len_new + 1}]抽奖->抽奖过了')
                    else:
                        print(f'[账号{self.len_new + 1}]抽奖->开始抽奖')
                        self.prize_draw()
        except Exception as e:
            print(e)



    # 抽奖
    def prize_draw(self):
        try:
            self.session.headers.update(
                {'Referer': 'https://srv-app.taizhou.com.cn/luckdraw/', 'Host': 'srv-app.taizhou.com.cn'})
            r1 = self.session.get('https://srv-app.taizhou.com.cn/tzrb/awardUpgrade/list?activityId=67').json()
            # print(r1)
            title_all = {}
            if 'code' in r1:
                if r1['code'] == 200:
                    for i in r1.get('data'):
                        # print(i)

                        title_all.update({i.get('id'): i.get('title')})
                    # print(title_all)
            time.sleep(random.randint(2, 4))
            b3 = 'activityId=67&sessionId=undefined&sig=undefined&token=undefined'
            self.session.headers.update({'Content-type': 'application/x-www-form-urlencoded'})
            r3 = self.session.post('https://srv-app.taizhou.com.cn/tzrb/userAwardRecordUpgrade/saveUpdate', data=b3).json()
            # print(r3)
            if r3.get('code') == 200:
                print(f'[账号{self.len_new + 1}]抽奖->' + title_all.get(r3.get("data")) + '    🎉')
            elif r3.get('code') == 500:
                print(f'[账号{self.len_new + 1}]抽奖->{r3.get("message")}')
            else:
                print(r3)
        except Exception as e:
            print(e)


def start(ce_token_all):
    try:
        prompt = VC().get_version('望潮')['prompt']
        Cloud_version = VC().get_version('望潮')['version']
        if Cloud_version > Current_version:
            print(f"当前版本:{Current_version}  最新版本:{Cloud_version}")
            print(f'最新更新内容:{prompt}')
            print("老版本跑没问题就无需更新脚本")
        else:
            print(f"当前版本:{Current_version}  最新版本:{Cloud_version}")
            print(f'最新更新内容:{prompt}')
        print(f"==========共找到{len(ce_token_all)}个账号==========")
        print('----------------------------个心信息----------------------------')
        for i in ce_token_all:
            Tide_reading(i, ce_token_all.index(i)).info()
        print('----------------------------任务查询----------------------------')
        for i in ce_token_all:
            Tide_reading(i, ce_token_all.index(i)).user_read_login()
        print('----------------------------抽奖查询----------------------------')
        for i in ce_token_all:
            Tide_reading(i, ce_token_all.index(i)).luck_draw()

    except:
        pass


def search_files():
    for root, dirs, files in os.walk(os.path.dirname(os.getcwd())):
        for file in files:
            if 'Tide_check.data.json' in file:
                return os.path.join(root, file)


if __name__ == '__main__':
    ce_token_all = json.load(open(search_files(), 'r'))['data']
    random.shuffle(ce_token_all)
    # print(ce_token_all)
    start(ce_token_all)
