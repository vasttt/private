# -*- coding:utf-8 -*-
"""
@Created on: 2024/5/3 17:15
@Author: zzzl
@Des；水位信息
"""
import time
from notify import send
import requests
access_token = ''
application_key = '948e22444de9458d8d14870a4bf4222a'
"""
@ cron: 9 8,17 * * *
@ new Env('水位信息')
@ 版本  1.0
"""

class Fishing_Helper:
    def __init__(self):
        ...

    def fishing_helper(self):
        headers = {
            'User-Agent': 'Mozilla/5.0 (Linux; Android 12; 2201122C Build/SKQ1.211006.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/116.0.0.0 Mobile Safari/537.36 XWEB/1160117 MMWEBSDK/20240301 MMWEBID/2389 MicroMessenger/8.0.48.2580(0x28003036) WeChat/arm64 Weixin NetType/WIFI Language/zh_CN ABI/arm64 MiniProgramEnv/android',
            'Host': 'www.qingxihuolang.com',
            'charset': 'utf-8',
        }
        new_timi = str(time.time() * 1000).split('.')[0]
        params = f'application_key={application_key}&request_timestamp={new_timi}&water_area_id=873&lever_info_record_amount=10'
        resp = requests.request('get','https://www.qingxihuolang.com/fishingHelper/waterArea/detail?', params=params, headers=headers)
        # print(resp.text)
        try:
            monitor_station_address = resp.json()['response_data']['water_area']['monitor_station_address']
            water_level_number = resp.json()['response_data']['water_level_info_list'][0]['water_level_number']
            rise_fall_number = resp.json()['response_data']['water_level_info_list'][0]['rise_fall_number']
            create_date_m_dd = resp.json()['response_data']['water_level_info_list'][0]['create_date_m_dd']
            print(f'测站地址:{monitor_station_address}\n日期:{create_date_m_dd}\n水位:{water_level_number}\n涨跌:{rise_fall_number}')
            send('今日水位', f'测站地址:{monitor_station_address}\n日期:{create_date_m_dd}\n水位:{water_level_number}\n涨跌:{rise_fall_number}')
        except ValueError:
            print('token过期')


def start():
    Fishing_Helper().fishing_helper()


if __name__ == '__main__':
    start()
